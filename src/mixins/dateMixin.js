import format from "date-fns/format";
import { ru } from "date-fns/locale";

export default {
  methods: {
    getFormatDate(value) {
      return format(value, "dd MMMM yyyy 'г.'", {
        locale: ru
      });
    }
  }
};
