export default {
  methods: {
    getFormatEvent(value) {
      switch (value) {
        case "add":
          return "Добавлено";
        case "delete":
          return "Удалено";
      }
    }
  }
};
