import Vue from "vue";
import Vuex from "vuex";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import Axios from "@/plugins/axios";

Vue.config.productionTip = false;
Vuex.Store.prototype.$api = Axios;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
