export default service => ({
  getPosts() {
    return service.get("https://jsonplaceholder.typicode.com/posts");
  }
});
