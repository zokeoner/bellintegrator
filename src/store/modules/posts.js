const state = () => ({
  posts: [],
  history: [],
  search: "",
  isLoading: true
});
// actions
const actions = {
  async GET_POSTS({ commit }) {
    commit("setState", {
      key: "isLoading",
      items: true
    });
    const posts = await this.$api.post.getPosts();
    posts.data.forEach(item => (item.hide = false));
    commit("setState", {
      key: "posts",
      items: posts.data
    });
    commit("setState", {
      key: "isLoading",
      items: false
    });
  },
  GET_SEARCH({ commit }, payload) {
    commit("setState", {
      key: "search",
      items: payload
    });
  },
  ADD_HISTORY({ commit, state }, payload) {
    const posts = JSON.parse(JSON.stringify(state.posts));
    const post = posts.findIndex(item => item.id === payload.id);
    posts[post].event = payload.event;
    posts[post].time = Date.now();
    commit("addHistory", posts[post]);
  },
  CHANGE_POST({ commit, state }, payload) {
    const posts = JSON.parse(JSON.stringify(state.posts));
    const post = posts.findIndex(item => item.id === payload);
    posts[post].hide = !posts[post].hide;

    commit("setState", {
      key: "posts",
      items: posts
    });
  }
};

// mutations
const mutations = {
  setState(state, { key, items }) {
    state[key] = items;
  },
  addHistory(state, post) {
    state.history = [...state.history, post];
  }
};
// getters
const getters = {
  getSearchPostTitle: state => {
    return state.posts.filter(
      item =>
        item.title.toLowerCase().indexOf(state.search.toLowerCase()) > -1 &&
        item.hide !== true
    );
  },
  getPostDelete: state => {
    return state.posts.filter(item => item.hide);
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
