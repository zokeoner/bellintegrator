import axios from "axios";
import PostService from "@/service/post.service";

const request = axios.create({
  baseURL: process.env.VUE_APP_API
});

const api = {
  post: PostService(request)
};

export default api;
